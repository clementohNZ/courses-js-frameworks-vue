import Home from './components/Home.vue'
import Header from './components/Header.vue'

const User = () => import('./components/user/User.vue')
const UserStart = () => import('./components/user/UserStart.vue')
const UserDetail = () => import('./components/user/UserDetail.vue')
const UserEdit = () => import('./components/user/UserEdit.vue')
const NotFoundPage = () => import('./components/user/User.vue')

export const routes = [
    {
        path: '',
        name: 'home',
        components: {
            default: Home,
            'header-top': Header
        }
    },
    {
        path: '/user',
        components: {
            default: User,
            'header-bottom': Header
        },
        children: [
            { path: '', component: UserStart },
            {
                path: ':id',
                component: UserDetail,
                beforeEnter: (to, from, next) => {
                    console.log('user detail before enter')
                    next()
                }
            },
            {path: ':id/edit', component: UserEdit, name: 'userEdit'},
        ]
    },
    {
        path: '/redirect-me',
        redirect: {
            path: '/user'
        }
    },
    {
        path: '*',
        component: NotFoundPage
    }
]